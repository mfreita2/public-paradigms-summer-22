console.log('js api demo page loaded');

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo; //

function getFormInfo(){ // this is triggered when the button is clicked
  // get name from user
  var name = document.getElementById('name-text').value;
  makeNetworkCallToAgeApi(name);

} // end of fn getFormInfo

function makeNetworkCallToAgeApi(name){
  var url = "https://api.agify.io/?name=" + name;

  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);

  // make nw call to agify api
  // get response from agify
  xhr.onload = function(e){ // this is triggered when the response for age api comes back
    // get the response from the api
    var response = xhr.responseText;
    console.log('response is ' + response);
    updateAgewithResponse(name, response);
  } // end of anonymous fn associated with onload

  xhr.onerror = function(e){
    // TODO
    console.error('error message is ' + xhr.statusText);
  } // end of anonymous fn associated with onerror

  xhr.send(null);
} // end of makeNetworkCallToAgeApi

function makeNetworkCallToNumbersApi(age){
  // TODO
}

function updateAgewithResponse(name, reponse){
  // update the UI with age
  var response_json = JSON.parse(response);
  var label1 = document.getElementById('response-line-1');

  if (response_json['age'] == null){
    var age = parseInt(response_json["age"]);
    var string_message = name + ", your predicted age is " + age + ".";
    label1.innerHTML = string_message;
    makeNetworkCallToNumbersApi(age);

  } else {
    label1.innerHTML = 'Apologies, no data.';
  }

  // make api call to numbers with age
  //makeNetworkCallToNumbersApi(age);
  // show result to UI
  // get age in a var
  // then make nw call to numbers with age
  // show trivia of number age

}
